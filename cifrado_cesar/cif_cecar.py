def crytospace(cripto):
    #retorna la posicion de los espacios
    lista=[(i) for i in cripto]
    index_espacio = ((list(filter(lambda x:cripto[x]==' ',range(len(cripto))))))
    return index_espacio
    
def desplaza_abc(abc, numero):
    # Desplaza el abcdario la cantida que le ingresa como argumento
    return abc[numero:]+ (abc[:numero])

def crytoList(cripto):
    #convierte un string a una lista con el espacio como separador
    return [(i) for i in cripto if i != ' ']


def listTostring(lista):
    #Convierte una lista a string
    string = ''
    for letra in lista:
        string = string + letra        
    return string

def indice_palabras(list_cripto):
    #retorna el indice de la posicion de la letra en la lista abc 
    for letra_cripto in list_cripto:    
        index.append((list(filter(lambda x:abcDesplazado[x]==letra_cripto,range(tam_abc))))[0])
    return index
    
#abc = 26
abc = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

#En esta parte se agrega la parte cifrada
cripto = 'UIF RVJDL CSPXO GPY KVNQT PWFS UIF MBAZ EPH PG DBFTBS BOE ZPVS VOJRVF TPMVUJPO JT ECMBDDESTPPM'

tam_abc = len(abc)
list_cripto = []
index=[]
mensaje_cesar_list = []

#convierte un string a una lista con el espacio como separador
list_cripto = crytoList(cripto)

# Desplaza el abcdario la cantida que le ingresa como argumento
abcDesplazado = desplaza_abc(abc,1)

#Crea la lista con todos los cambios  y desplazamientos realizados
for indice in indice_palabras(list_cripto):     
    mensaje_cesar_list.append(abc[indice])

#Agrega los espacios de acuerdo a la posicion correspondiente y los añade a la lista
for posicion in crytospace(cripto):
    mensaje_cesar_list.insert(posicion,' ')
    
#convierte la lista a una cadena de texto y lo muestra en el terminal      
print(listTostring(mensaje_cesar_list))






