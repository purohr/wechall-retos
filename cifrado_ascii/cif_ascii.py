from bs4 import BeautifulSoup
from requests import get
import creartxt
import os
import pandas as pd
def get_html():
    url = "https://www.asciitable.xyz/"
    # Capturamos el hml de la pagina web y creamos un objeto Response
    r  = get(url)
    #data = r.text    
    #si el request tuvo exito
    status_code = r.status_code
    if status_code == 200:        
        # Creamos el objeto soup y le pasamos lo capturado con request
        #soup = BeautifulSoup(data, 'lxml') 
        #lee la tabla directamente de la pagina y convierte la tabla en un data frame
        soup=pd.read_html(url)[0]

        # Capturamos el titulo de la página y luego lo mostramos
        # Lo que hace BeautifulSoup es capturar lo que esta dentro de la etiqueta title de la url
        
        #print (atributo_full)        
    else:
        print ("Status Code %d" % status_code) 
    
    return soup


def get_data(soup):   
    data = []
    table = soup.find('table')
    table_body = table.find('tbody')    
    rows = table.find_all('tr')   
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols if ele]) # Get rid of empty values
    return data


cifrado= '84, 104, 101, 32, 115, 111, 108, 117, 116, 105, 111, 110, 32, 105, 115, 58, 32, 102, 99, 108, 115, 112, 98, 102, 100, 115, 97, 97, 108'

soup = get_html()
#list_data_unclean = get_data(soup)
filename='ascii.csv'
if not os.path.isfile(filename):
    if creartxt.crear_txt(filename):
        creartxt.agregar_txt(filename,soup)
        mensaje_descifrado=creartxt.leer_txt(filename,cifrado)
        #for sublist in list_data_unclean:
         #   creartxt.agregar_txt("ascii.csv", ','.join(sublist))
    else:
        print (" Hay algún error procesando el archivo. REVISAR")    
else:
    mensaje_descifrado=creartxt.leer_txt(filename,cifrado)
    
print(mensaje_descifrado)
        
        
        






"""
https://www.ascii-code.com/

https://www.rapidtables.com/code/text/ascii-table.html
"""
