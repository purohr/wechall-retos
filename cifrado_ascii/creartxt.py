#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import pandas as pd


def makedir (folder = "convertido-txt"):
	patch = os.getcwd()
	#folder = "convertido-txt"	
	new_folder = patch + '/' + folder
	try:
		os.mkdir(folder)
		if os.path.isdir(new_folder):
			os.chdir(new_folder)
			return True, new_folder

		else:
			return False, new_folder

	except FileExistsError:
		print("directorio ya existe")
		os.chdir(new_folder)
		return True, new_folder


def file_exist(path): # el archivo existe?
	return os.path.isfile(path)

def crear_txt(nom_archivo):
	f = open(nom_archivo, "w")
	f.close()

	if file_exist(os.getcwd() + '/' + nom_archivo):
		return True
	else:
		return False
		
def busca_caracter(datos_df,caracter):
    fila = datos_df[datos_df['Dec'] == caracter]
    caracter_ascii= fila.loc[caracter,'Char']
    return caracter_ascii

def mensaje_full(remplace_item_list):
#reemplaza 'space' por un ' ' espacio verdadero 
    return ''.join([' ' if x == 'space' else x for x in remplace_item_list])		

def agregar_txt(nom_archivo, texto_df):
	#str(texto)
	texto_df.to_csv(nom_archivo, index = False, header=True)
	#f = open(nom_archivo,"a")
	#f.write(texto + "\n")
	#f.close()
	return 

def leer_txt(filename,cifrado):
    datos_df = pd.read_csv(filename)
    cif_list=cifrado.split(',')
    descifradoAscii=[]
    for cifrado in cif_list:
        descifradoAscii.append(busca_caracter(datos_df,int(cifrado)))
    mensaje_descifrado=mensaje_full(descifradoAscii)
    return mensaje_descifrado
## programa


