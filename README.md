# wechall-retos

Retos de la pagina www.wechall.net desarrollados en Python

* Cifrado Cesar. [https://es.wikipedia.org/wiki/Cifrado_César]
* Orden decendente. [Organiza los numeros de mayor a menor]
* Comandos en el terminal [Ejemplo de como ejecutar comandos o archivos ene l terminal]
* Cifrado ascii [codigo para descifrar mensajes en ascii]
* Decodificación URL [Pagina utilizada para descifrar codificación de url]
